import React, { useEffect, useState } from "react";
import { FiPower, FiTrash2 } from "react-icons/fi";
import { Link, useHistory } from "react-router-dom";
import logoImg from "../../assets/logo.svg";
import api from "../../services/api";
import "./styles.css";

export default function Profile() {
	const [incidents, setIncidents] = useState([]);

	const ongName = localStorage.getItem("ongName");
	const ongId = localStorage.getItem("ongId");
	const history = useHistory();

	useEffect(() => {
		api.get("/profile", { headers: { Authorization: ongId } }).then(res => {
			setIncidents(res.data);
		});
	}, [ongId]);

	const handleDeleteIncident = async id => {
		try {
			await api.delete(`/incident/${id}`, { headers: { Authorization: ongId } });

			setIncidents(incidents.filter(i => i.id !== id));
		} catch (error) {
			alert("Erro ao deletar o caso");
		}
	};

	const handleLogout = () => {
		localStorage.clear();
		history.push("/");
	};

	return (
		<div className="profile-content">
			<header>
				<img src={logoImg} alt="be the Hero" />
				<span>Bem vindo {ongName}</span>

				<Link className="button" style={{ marginTop: 0 }} to="/incidents/new">
					Cadastrar novo caso
				</Link>

				<button type="button" onClick={() => handleLogout()}>
					<FiPower size={18} color="#e02041" />
				</button>
			</header>

			<h1>Casos cadastrados</h1>

			<ul>
				{incidents.map(item => (
					<li key={item}>
						<strong>Caso</strong>
						<p>{item.title}</p>
						<strong>Descricao</strong>
						<p>{item.description}</p>
						<strong>Valor</strong>
						<p>{Intl.NumberFormat("pt-br", { style: "currency", currency: "BRL" }).format(item.value)}</p>

						<button type="button" onClick={e => handleDeleteIncident(item.id)}>
							<FiTrash2 size={20} color="#a8a8b3" />
						</button>
					</li>
				))}
			</ul>
		</div>
	);
}
